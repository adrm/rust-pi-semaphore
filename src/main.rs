extern crate wiringpi;

extern crate iron;
#[macro_use]
extern crate router;
extern crate logger;

use std::thread;
use std::sync::mpsc;
use std::sync::Mutex;

mod server;
mod hardware;

fn main() {
    println!("----> Arrancando semáforo...");
    let (tx, rx) = mpsc::sync_channel(0);

    let hardware_tx = tx.clone();
    thread::spawn(|| {
        hardware::watch_button(Mutex::new(hardware_tx));
    });

    let server_tx = tx.clone();
    thread::spawn(||  {
        server::init_server(Mutex::new(server_tx));
    });

    hardware::inicial();

    loop {
        println!("Semáforo en posición inicial.");
        rx.recv().unwrap();
        hardware::boton_handler();
    }
}
