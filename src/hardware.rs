use std::sync::mpsc::SyncSender;
use std::sync::Mutex;
use std::thread::sleep;
use std::time::Duration;
use wiringpi;
use wiringpi::pin::Value::{High, Low};

struct Luz {
    gpio: wiringpi::pin::OutputPin<wiringpi::pin::Gpio>
}

struct Semaforo {
    verde: Luz,
    ambar: Luz,
    rojo: Luz
}

struct Paso {
    peatones: Semaforo,
    vehiculos: Semaforo,
}

impl Paso {
	pub fn default(pi: wiringpi::WiringPi<wiringpi::pin::Gpio>, p_verde: u16, p_rojo: u16, v_verde: u16, v_ambar: u16, v_rojo: u16) -> Paso {
		Paso {
	        peatones: Semaforo {
	            verde: Luz {gpio: pi.output_pin(p_verde)},
	            rojo: Luz {gpio: pi.output_pin(p_rojo)},
	            ambar: Luz {gpio: pi.output_pin(100)}, // Valor no válido
	        },
	        vehiculos: Semaforo {
	            verde: Luz {gpio: pi.output_pin(v_verde)},
	            rojo: Luz {gpio: pi.output_pin(v_rojo)},
	            ambar: Luz {gpio: pi.output_pin(v_ambar)},
	        },
	    }
	}
}

impl Luz {
    fn apagar(&self) {
		self.gpio.digital_write(Low);
    }

    fn encender(&self) {
		self.gpio.digital_write(High);
    }
}

impl Semaforo {
    fn encender_rojo(&self) {
        self.verde.apagar();
        self.ambar.apagar();
        self.rojo.encender();
    }

    fn encender_verde(&self) {
        self.rojo.apagar();
        self.ambar.apagar();
        self.verde.encender();
    }

    fn encender_ambar(&self) {
        self.rojo.apagar();
        self.verde.apagar();
        self.ambar.encender();
    }
}


pub fn boton_handler() {
	println!("Pulsador del semáforo activado.");
	let paso = Paso::default(wiringpi::setup_gpio(), 23, 22, 24, 18, 17);

	paso.vehiculos.encender_ambar();
	sleep(Duration::new(1, 0));
	paso.vehiculos.encender_rojo();
	sleep(Duration::new(1, 0));
	paso.peatones.encender_verde();
	sleep(Duration::new(3, 0));
	paso.peatones.encender_verde();
	paso.vehiculos.encender_ambar();
	parpadear(&vec![&paso.peatones.verde, &paso.vehiculos.ambar], 3);
	paso.peatones.encender_rojo();
	parpadear(&vec![&paso.vehiculos.ambar], 1);
	paso.vehiculos.encender_verde();
}

fn parpadear(luces: &Vec<&Luz>, segundos: u16) {
	let parpadeos_por_medio_segundo = 2;
	for _ in 0..segundos {
		for _ in 0..parpadeos_por_medio_segundo {
			for luz in luces {
				luz.apagar();
			}
			sleep(Duration::from_millis(500/parpadeos_por_medio_segundo));
			for luz in luces {
				luz.encender();
			}
			sleep(Duration::from_millis(500/parpadeos_por_medio_segundo));
		}
	}
}

pub fn watch_button(tx: Mutex<SyncSender<()>>) {
	let pi = wiringpi::setup_gpio();
	let pin = pi.input_pin(4);

	loop {
		if pin.digital_read() == Low {
			match tx.lock().unwrap().try_send(()) {
				Ok(_)	=> println!("Botón físico pulsado"),
				Err(err)=> println!("¡Quieto! {}", err)
			}
		}
		sleep(Duration::from_millis(200));
	}
}

pub fn inicial() {
	let paso = Paso::default(wiringpi::setup_gpio(), 23, 22, 24, 18, 17);
	paso.vehiculos.encender_verde();
	paso.peatones.encender_rojo();
}
