use iron::prelude::*;
use iron::status;

use logger::Logger;
use logger::format::Format;

use std::sync::mpsc::SyncSender;
use std::sync::Mutex;

static FORMAT: &'static str =
        "{method} {uri} -> {status} ({response-time})";


pub fn init_server(tx: Mutex<SyncSender<()>>) {
	    let router = router!{
            get ""          => |_: &mut Request| {
                Ok(Response::with((status::Ok, "Servidor funcionando!")))
            },
	        get "/boton"    => move |_: &mut Request| {
                match tx.lock().unwrap().try_send(()) {
    				Ok(_)	=> Ok(Response::with((status::Ok, "Pulsación recogida."))),
    				Err(_)  => Ok(Response::with((status::Ok, "Prueba de nuevo.")))
    			}
            },
	        get "/estado"   => estado_web,
	    };

	    let format = Format::new(FORMAT, vec![], vec![]);
	    let (logger_before, logger_after) = Logger::new(Some(format.unwrap()));

	    let mut chain = Chain::new(router);
	    chain.link_before(logger_before);
	    chain.link_after(logger_after);

	    let server = Iron::new(chain).http("0.0.0.0:8080");
	    println!("Servidor web del semáforo levantado en el puerto 8080.");
        server.unwrap();

        fn estado_web(_: &mut Request) -> IronResult<Response> {
        	Ok(Response::with((status::Ok, "No implementado.")))
        }

}
